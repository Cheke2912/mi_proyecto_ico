//Programa Empresa

#include <stdio.h>

int main(int argc, char const *argv[]) {

int piezas = 0;
int precio = 0;
float salario = 0.0f;
float descuento = 0.0f;

printf("¿Cuantas piezas vendimos? ");
  scanf("%d", &piezas );

//Comisión
if ((piezas>=1) && (piezas<=12)) {  //Si se venden de 1 a 12 descuento del 5%
  descuento = .5;
}else{
 if ((piezas>=13) && (piezas<=30)) { //Si se venden de 13 a 30 descuento del 7%
  descuento = .7;
} else {
 descuento = .10;  //Si son mas de 30 descuento de 10%
}
}


//Precio por pieza
if ((piezas>=1) && (piezas<=24)) {
  precio = 450;
}else{
  if (piezas>=25) {
  precio = 435;
}
}

//Salario final para trabajadores
if ((piezas>=1) && (piezas<=12)) {
  salario = (piezas*precio)*descuento;
}

if ((piezas>=13) && (piezas<=24)) {
  salario = (piezas*precio)*descuento;
}

if ((piezas>=25) && (piezas<=30)) {
  salario = (piezas*precio)*descuento;
}

if (piezas>=31) {
  salario = (piezas*precio)*descuento;
}

printf("El salario es de: %.2f por que vendimos %d piezas\n", salario, piezas );

  return 0;
}
