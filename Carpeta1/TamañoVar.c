#include <stdio.h>
int main(int argc, char const *argv[]) {

//Declaramos todas nuestras variables

char letra = 'A';
int entero = 1;
float fl = 2.0;
long int lin = 18;

//Declarar valores
printf("El valor actual de de char: %c\n",letra );
printf("El valor actual de de entero: %d\n",entero );
printf("El valor actual de de float: %f\n",fl );
printf("El valor actual de de long int: %lu\n",lin );

//Tamaño de var
printf("La variable char/letra ocupa %lu bytes en memoria\n", sizeof(letra)  );
printf("La variable int/entero ocupa %lu bytes en memoria\n", sizeof(entero) );
printf("La variable float/flotante ocupa %lu bytes en memoria\n", sizeof(fl) );
printf("La variable double/doble ocupa %lu bytes en memoria\n", sizeof(lin) );

  return 0;
}
