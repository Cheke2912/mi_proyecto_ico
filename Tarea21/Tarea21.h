struct auto {

  int modelo;
  int clase;
  char marca[20];
  char paisOrigen[20];
  float kilometraje;


};

struct computadora {

  int modelo;
  int RAM;
  char marca[20];
  char procesador[20];
  char SO[10] ;


};

struct alumnos {

  int edad;
  char nombre[60];
  char genero;
  char carrera[25];
  char nCuenta[10];

};

struct equipoFutbol {

  int noJugadores;
  int noCamisetas;
  char equipo[15];
  char liga[10];
  char nombreEntrenador;


};

struct mascota {

  int edad;
  int noMascotas;
  char nombre[10];
  char raza[20];
  float peso;


};

struct VideoJuego {

  int añoPublicacion;
  int noJugadores;
  char nombre[20];
  char paisOrigen[20];
  float pesoEnGB;


};

struct superHeroe {

  int edad;
  int personasSalvadas;
  char alias[20];
  char nombreEnemigo[20];
  float estatura;


};

struct planeta {

  int noEnSistemaSolar;
  int personasHabitantes;
  char nombre[20];
  char alias[20];
  float dimension;


};

struct personaje {

  int misionesHechas;
  int nivel;
  char alias[20];
  char nombre[20];
  float ptsHabilidad;


};

struct comic {

  int edicion;
  int coleccion;
  char marca[20];
  char DCoMarvel[20];
  float noSerie;


};
